/*additional notes for express
	steps on how to use express
	npm init- initialize the node package manager
	npm install express- will install express
		other ways to install express:
			install express@<version>
		uninstall the package
			uninstall express
	make a file called .gitignore
		inside that file, type node_modules
		this is to prevent it for being added when pushing our repository
	install nodemon
		this is for running the server continuously.
		in package.json file, find scripts, under test, put "start" : "nodemon index"
	run your server, using npm start*/

// require directive tells us to load the express module
const express = require('express');

// creating a server using express
const app = express();

// port
const port = 4000;

// middlewares
// allows up to read a json data
app.use(express.json());
/*allows app to read data from forms
by default, information received from the url can only be received as string or an array
with extended: true, this allows to receive information in other data types such as objects
*/
app.use(express.urlencoded({extended: true}));

// mock database
let users = [
	{
		email: "nezukoKamado@gmail.com",
		username: "nezuko01",
		password: "letMeOut",
		isAdmin: false
	},
	{
		email: "tanjiroKamado@gmail.com",
		username: "gopanchiro",
		password: "iAmTanjiro",
		isAdmin: false
	},
	{
		email: "zenitsuAgatsuma@gmail.com",
		username: "zenitsuSleeps",
		password: "iNeedNezuko",
		isAdmin: true
	}
]

let loggedUser;

// GET METHOD
/*
 app -server
 get - HTTP method
 / - routename or endpoint
 (req, res) - resquest and response will handle the requests ans the responses
 res.send - combines writeHead() and end(), used to send response to our client
 */
app.get('/', (req, res) => {
	res.send('Hello World')
});

/*
	Mini activity
	make a route with /hello as an endpoint
	send a message that says "Hello from Batch 131"
*/
app.get('/hello', (req, res) => {
	res.send('Hello from Batch 131')
});


// POST METHOD
// use console.log to check what's inside the request body
app.post('/', (req, res) => {
	console.log(req.body);
	res.send(`Hello I am ${req.body.name}, I am ${req.body.age}, I could be described as ${req.body.description}`)
})

/*
	mini-activity
	change the message that we send: "Hello I am <name>, I am <age>, I could be described as <description>"
*/

// USER REGISTER route
app.post('/users/register', (req, res) => {
	console.log(req.body);

	let newUser = {
		email: req.body.email,
		username: req.body.username,
		password: req.body.password,
		isAdmin: req.body.isAdmin
	}

	users.push(newUser)
	console.log(users);

	res.send(`User ${req.body.username} has successfully registered.`)
})

// USER LOGIN route
app.post('/users/login', (req, res) => {
	// should contain username and password
	console.log(req.body);

	// find user with matching username and password
	let foundUser = users.find((user) => {
		return user.username === req.body.username && user.password === req.body.password;
	});

	if(foundUser !== undefined){

		// get the index number of the foundUser, but since the users array is an array of objects, we have to use findIndex(), this will iterate over all the items and return the index number of the current item that matches the return condition. It is similar to find() but instead it will return only the index number.
		let foundUserIndex = users.findIndex((user) => {
			return user.username === foundUser.username
		});

		// This will add the index of your found user in the foundUser object
		foundUser.index = foundUserIndex;

		// temporarily log our user in, this allows us to refer the details of a logged in user
		loggedUser = foundUser;

		// show all the properties of the user including the index
		console.log(foundUser)
		res.send('Thank you for logging in.')
	} else {
		loggedUser = foundUser;
		res.send('Logon Failed, wrong credentials.')
	}
})

// CHANGE PASSWORD Route
app.put('/users/change-password', (req, res) => {

	// store the message that will be sent back to our client
	let message;

	// will loop through all the "users" array
	for(let i = 0; i < users.length; i++){

		// if the username provided in the request is the same with the username in the loop
		if(req.body.username === users[i].username){

			// change the password of the user found in the loop by the requested password in the body by the client
			users[i].password = req.body.password;

			// send a message to the client
			message = `User ${req.body.username}'s password has been changed.`;

			// break the loop once a user matches the username provided in the client
			break;

			// if no user was found
		} else {
			// changes the message to be sent back as a response
			message = `User not found.`
		}
	}

	// response that will be sent to our client
	res.send(message);
})


/*ACTIVITY*/
// HOME route
app.get('/home', (req, res) => {
	res.send('This is your homepage')
});

// USERS route
app.get('/users', (req, res) => {
	res.type('application/json')
	res.send(JSON.stringify(users))
});

// DELETE route
app.delete('/delete-user', (req, res) => {
	let message;

	for(let i = 0; i < users.length; i++){
		if(req.body.username === users[i].username){
			message = `User ${req.body.username} has been deleted`;
			break;
		} else {
			message = `User not found.`
		}
	}
	res.send(message);
})

// listen to the port and returning message in the terminal
app.listen(port, () => console.log(`The Server is running at port ${port}`));
